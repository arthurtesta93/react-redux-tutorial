import React, {Component} from 'react'

class MemeGenerator extends Component{
    constructor(){
        super()
        this.state = {
            toptext: '',
            bottomtext: '',
            randomImg: 'http://i.imgflip.com/1bij.jpg',
            allMemeImgs: []
        }    
    }

    componentDidMount(){
        fetch("https://api.imgflip.com/get_memes")
            .then(response => response.json())
            .then(response => {
                const {memes} = response.data
                this.setState({allMemeImgs: memes})
        })
    }       

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({ [name]: value})
    }

    handleSubmit = (event) => {
     event.preventDefault()   
     const randNum = Math.floor(Math.random() * this.state.allMemeImgs.length)
     const newImgUrl = this.state.allMemeImgs[randNum].url   
     this.setState({randomImg: newImgUrl})
     
    }

    render(){
        return(
            <div>
                <form className="meme-form" onSubmit={this.handleSubmit}>
                    <input 
                    name="toptext" 
                    type="text" 
                    placeholder="Top text"
                    value={this.state.toptext}
                    onChange={this.handleChange}
                    />
                    <br/>
                    <input 
                    name="bottomtext" 
                    type="text" 
                    placeholder="Bottom text"
                    value={this.state.bottomtex}
                    onChange={this.handleChange}
                    />
                    <br/>
                    <button>Gen</button>
                </form>
                <div className="meme">
                    <img src={this.state.randomImg} alt=""/>
                    <h2 className="top">{this.state.toptext}</h2>
                    <h2 className="bottom">{this.state.bottomtext}</h2>
                </div>
            </div>
        )
    }
    
}

export default MemeGenerator