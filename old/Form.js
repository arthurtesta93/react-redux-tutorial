import React, {Component} from 'react';

class Form extends Component  {
  constructor () {
    super()
    this.state = {
      firstName: "",
      lastName: "",
      age: "",
      gender: "",
      location:"",
      isVegan: false,
      isKosher: false,
      isGlutenFree: false, 
     
    }

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event){

    const {name, value, type, checked} = event.target
    
    type === "checkbox" ? 
        this.setState({
          [name]: checked
        })
        :
        this.setState({
          [name]: value
        })
      }

  handleSubmit(){
    alert()
  }

  render() {
    return (
     <main>
      <form onSubmit={this.handleSubmit}>
          <input 
          name="firstName"
          value={this.state.firstName}
          onChange={this.handleChange}
          placeholder="First Name"
          />

           <br /> 
          
          <input
          name="lastName"
          value={this.state.lastName}
          onChange={this.handleChange}
          placeholder="Last Name"
          />

          <br/> 

          <input
          name="age"
          value={this.state.age}
          onChange={this.handleChange}
          placeholder="Age"
          />

          <br/>

          <label>
            <input
            type="radio"
            name="gender"
            value="male"
            checked={this.state.gender === "male"}
            onChange={this.handleChange}  
            /> Male
             <br/>
            <input
            type="radio"
            name="gender"
            value="female"
            checked={this.state.gender === "female"}
            onChange={this.handleChange}  
            /> Female

          </label>
          <br/>
           <select 
           value={this.state.location} 
           name="location"
           onChange={this.handleChange}
           >
              <option value="">Please choose your destination</option>
              <option value="Los Angeles">Los Angeles</option>
              <option value="Chicago">Chicago</option>
              <option value="New York">New York</option>
           </select>
          <br />

          <label>
              <input
                type="checkbox"
                name="isVegan"
                onChange={this.handleChange}
                checked={this.state.isVegan}
              /> Vegan?
          </label>
          <br/>
          <label>
              <input
                type="checkbox"
                name="isKosher"
                onChange={this.handleChange}
                checked={this.state.isKosher}
              /> Kosher?
          </label>
          <br/>
          <label>
              <input
                type="checkbox"
                name="isGlutenFree"
                onChange={this.handleChange}
                checked={this.state.isGlutenFree}
              /> Gluten Free?
          </label> 
          <br/>

        <button>Submit</button>
     </form>
     <h1>Entered information</h1>
     <p>Your Name: {this.state.firstName}  {this.state.lastName}</p>
     <p>Your age: {this.state.age}</p> 
     <p>Your gender: {this.state.gender}</p> 
     <p>Your destination: {this.state.location}</p>
     <p>Vegan ? {this.state.isVegan ? "Yes" : "No"}     </p>
     <p>Kosher ? {this.state.isKosher ? "Yes" : "No"}     </p>
     <p>Gluten Free ? {this.state.isGlutenFree ? "Yes" : "No"}     </p>
     </main>
    )
  }
}

export default Form;
